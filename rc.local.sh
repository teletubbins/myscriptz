iptables -A OUTPUT -m state --state ESTABLISHED, RELATED -j ACCEPT 
iptables -A OUTPUT -d 127.0.0.1 -j ACCEPT 
iptables -A OUTPUT -p tcp -m tcp --dport 21 -j ACCEPT 
iptables -A OUTPUT - p tcp -m tcp --dport 22 -j ACCEPT 
iptables -A OUTPUT -p tcp -m tcp --dport 53 -j ACCEPT 
iptables -A OUTPUT -p tcp -m tcp --dport 80 -j ACCEPT 
iptables -A OUTPUT -p tcp -m tcp --dport 443 -j ACCEPT 
iptables -A OUTPUT -p tcp -m tcp --dport 10000 -j ACCEPT 
iptables -A OUTPUT -p udp -m udp --dport 53 -j ACCEPT 
iptables -A OUTPUT -p udp -m udp -j DROP 
iptables -A OUTPUT -p tcp -m tcp -j DROP