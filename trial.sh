#!/bin/bash
#Script auto create trial user SSH
#will expired after 1 day


MYIP=`dig +short myip.opendns.com @resolver1.opendns.com`

Login=trial`</dev/urandom tr -dc X-Z0-9 | head -c4`
hari="1"
Pass=`</dev/urandom tr -dc a-f0-9 | head -c9`

useradd -e `date -d "$hari days" +"%Y-%m-%d"` -s /bin/false -M $Login
echo -e "$Pass\n$Pass\n"|passwd $Login &> /dev/null
echo -e ""
echo -e "\e[0m                                                   "
echo -e "\e[94m[][][]======================================[][][]"
echo -e "\e[0m                                                   "
echo -e "\e[93m           AutoScriptVPS by Teletubbins           "
echo -e "\e[0m                                                   "
echo -e "         Host      :  $MYIP"
echo -e "         OpenSSH   :  22, 143"
echo -e "         Dropbear  :  80, 443"
echo -e "         SSL       :  442"
echo -e "         Squid     :  8080, 3128"
echo -e "         OpenVPN   :  http://$MYIP:81/client.ovpn"
echo -e "\e[0m                                                   "
echo -e "         Username  :  $Login"
echo -e "         Password  :  $Pass\n"
echo -e ""
echo -e "\e[94m[][][]======================================[][][]\e[0m"
