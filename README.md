## Service ##

- OpenSSH  : 22, 143
- Dropbear : 80, 443
- SSL      : 442
- Squid3   : 8080, 3128
- OpenVPN  : TCP 1194 
(Config : http://ip_vps:81/client.ovpn)
- badvpn   : badvpn-udpgw port 7300


## Script ##

- menu         (Menu List)
- create       (Create Account)
- add          (Add Account)
- delete       (Delete User)
- check        (Check User)
- member       (Check Member)
- expired      (Expired Account)

## Fitur lain ##

- DDoS Deflate
- Webmin
- Timezone : Asia/Jakarta


## Installation ##

Copy and paste the command and hit ENTER. Wait until the process is done.

`apt-get update && apt-get install ca-certificates && wget https://bitbucket.org/teletubbins/myscriptz/raw/8689ca1b5d3bc19fde0fc28ca3a2d32326a69b82/autovps.sh && chmod +x autovps.sh && ./autovps.sh`


 



## Modified by `Teletubbins`



















